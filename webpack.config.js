// 由于 webpack 是基于Node进行构建的，所有，webpack的配置文件中，任何合法的Node代码都是支持的

const path = require('path');

// 在内存中，根据指定的模板页面，生成一份内存中的首页，同时自动把打包好的bundle注入到页面底部
// 如果要配置插件，需要在导出的对象中，挂载一个 plugins 节点
const webpack = require('webpack');

const htmlWebpackPlugin=require('html-webpack-plugin');

//分离CSS
const ExtractTextPlugin = require("extract-text-webpack-plugin");

//压缩JS
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

// 向外暴露配置对象
module.exports = {

    // 入口
    entry:{
        //应用代码
        main : path.join(__dirname, './src/main.js'),
        //公共代码
        vendor:['vue','vuex','vue-router','vue-resource','vue-preview','moment','mint-ui']
    },
    // 输出
    output: {
        // path: path.resolve(__dirname, './dist'), // 输出路径
        path: path.join(__dirname, './dist'), // 输出路径
        // filename: 'bundle.js' //指定输出文件的名称
        // filename: 'bundle.[hash:6].js'
        // filename:'[name].js'
        filename: 'js/[name].[hash:6].js'
    },
    devServer: {
        open:true,
        port:3000,
        // contentBase:'src',
        hot:true
    },
    // 所有webpack  插件的配置节点
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new htmlWebpackPlugin({
            template:path.join(__dirname,'./src/index.html'),//指定模板文件路径
            filename:'index.html' //设置生成的内存页面的名称
        }),
        
        new ExtractTextPlugin("css/styles.[contenthash:6].css"),


        new webpack.optimize.CommonsChunkPlugin({
            // manifast 记录使用者和第三方依赖包的关系
            names:['vendor','manifast']
        }),

        //压缩JS
        new UglifyJsPlugin(),

        //删除警告
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })


    ],
    // 配置所有第三方loader 模块的
    module: {
        rules: [
            // 处理 CSS 文件的 loader
            //{ test : /\.css$/, use: ['style-loader','css-loader']},
            { test : /\.css$/, use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: "css-loader"
            })},
            // 处理 less 文件的 loader
            { test : /\.less$/, use: ['style-loader','css-loader', 'less-loader']},
            // 处理 sass 文件的 loader
            { test : /\.scss$/, use: ['style-loader','css-loader', 'sass-loader']},
            // 处理 图片路径的 loader
            // limit 给定的值，是图片的大小，单位是 byte， 如果我们引用的 图片，大于或等于给定的 limit值，则不会被转为base64格式的字符串， 如果 图片小于给定的 limit 值，则会被转为 base64的字符串
            // { test : /\.(jpeg|jpg|png|gif|bmp)$/, use:'url-loader?limit=595284&name=[hash:8]-[name].[ext]'},
            { test : /\.(jpeg|jpg|png|gif|bmp)$/, use:'url-loader?limit=4096&name=assets/[hash:8]-[name].[ext]'},            
            // 处理 字体文件的 loader
            { test : /\.(ttf|eot|svg|woff|woff2)$/, use: 'url-loader' },
            // 配置 Babel 来转换高级的ES语法
            { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
            // 处理 vue
            // { test : /\.vue$/, use: 'vue-loader'}
            { test: /\.vue$/, use: 'vue-loader' } // 处理 .vue 文件的 loader
        ]
    },
    /*resolve: {babel
        alias: { // 修改 Vue 被导入时候的包的路径
            "vue$": "vue/dist/vue.js"
        }
    }*/
};