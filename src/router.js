import VueRouter from 'vue-router'

// 导入对应的路由组件

// const Foo = () => import('./Foo.vue')
//路由懒加载
const home  = () => import(/* webpackChunkName: "group-foo" */'./components/tabbar/homeContainer.vue')
const user  = () => import(/* webpackChunkName: "group-foo" */'./components/tabbar/userContainer.vue')
const cart  = () => import(/* webpackChunkName: "group-foo" */'./components/tabbar/cartContainer.vue')
const search  = () => import(/* webpackChunkName: "group-foo" */'./components/tabbar/searchContainer.vue')
const newlist  = () => import(/* webpackChunkName: "group-foo" */'./components/new/newlist.vue')
const newdetail  = () => import(/* webpackChunkName: "group-foo" */'./components/new/newdetail.vue')
const photolist  = () => import(/* webpackChunkName: "group-foo" */'./components/photo/photolist.vue')
const photoinfo  = () => import(/* webpackChunkName: "group-foo" */'./components/photo/photoinfo.vue')
const goodslist  = () => import(/* webpackChunkName: "group-foo" */'./components/shop/goodslist.vue')
const goods  = () => import(/* webpackChunkName: "group-foo" */'./components/shop/goods.vue')
const goodsDesc  = () => import(/* webpackChunkName: "group-foo" */'./components/shop/goodsDesc.vue')
const goodsComment  = () => import(/* webpackChunkName: "group-foo" */'./components/shop/goodsComment.vue')
const reg  = () => import(/* webpackChunkName: "group-foo" */'./components/user/reg.vue')
const login  = () => import(/* webpackChunkName: "group-foo" */'./components/user/login.vue')

/* import home from './components/tabbar/homeContainer.vue'
import user from './components/tabbar/userContainer.vue'
import cart from './components/tabbar/cartContainer.vue'
import search from './components/tabbar/searchContainer.vue'
import newlist from './components/new/newlist.vue'
import newdetail from './components/new/newdetail.vue'
import photolist from './components/photo/photolist.vue'
import photoinfo from './components/photo/photoinfo.vue'
import goodslist from './components/shop/goodslist.vue'
import goods from './components/shop/goods.vue'
import goodsDesc from './components/shop/goodsDesc.vue'
import goodsComment from './components/shop/goodsComment.vue'
import reg from './components/user/reg.vue'
import login from './components/user/login.vue' */

var router = new VueRouter({
    routes: [
        { path: '/', redirect: '/home' },
        { path: '/home', component: home},
        { path: '/user', component: user ,name:'user'},
        { path: '/cart', component: cart ,name:'cart' },
        // { path: '/search', component: search },        
        { path: '/newlist', component: newlist },
        { path:'/newlist/:id' ,component:newdetail},
        { path: '/home/photos' ,component:photolist},
        { path: '/home/photos/:id',component:photoinfo},
        { path:'/goods', component:goodslist},
        { path:'/goods/:id', component:goods , name : 'goods'}, // 可以根据名称跳转到路由
        { path:'/goodsDesc/:id', component:goodsDesc , name : 'goodsDesc'},
        { path:'/goodsComment/:id', component:goodsComment , name : 'goodsComment'},
        { path:'/user/reg',component:reg ,name:'reg'},
        { path:'/user/login',component:login ,name:'login'}
    ],
    linkActiveClass:'mui-active', // 修改路由高亮样式类，默认的类是 router-link-active
    scrollBehavior(){
       
    }
})
router.beforeEach((to,from,next) => {
    //如果未匹配到路由
    if (to.matched.length ===0) {        
        //如果上级也未匹配到路由则跳转登录页面，如果上级能匹配到则转上级路由                                
        from.name ? next({ name:from.name }) : next('/');   
      } else {
          //如果匹配到正确跳转
        next();         
      }
})
router.afterEach((to,from,next) => {
    //window.scrollTop(0,0);
    //滚动到顶部
    if(document.querySelector(".main-container"))
        document.querySelector(".main-container").scrollIntoView();
});

export default router