let GoodsTool = {}

var GoodsList = {} //从 localStorange 获取

//添加商品
GoodsTool.add = function (goods){
    if(GoodsList[goods.id]){
        GoodsList[goods.id] += goods.count; 
    }else{
        GoodsList[goods.id] = goods.count; 
    }
}
//删除商品
GoodsTool.delete = function (id){
    delete GoodsList[id];
}
//获取商品信息
GoodsTool.getGoods = function (id){
    return GoodsList[id];
}
//获取商品总数量
GoodsTool.getTotalCount = function (){
    var sum = 0;
    for (i in GoodsList) {       
        sum+=GoodsList[i];
    }
    return sum;
}


// 向外暴露这个对象
export default GoodsTool;
