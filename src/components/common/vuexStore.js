// 导入Vuex
import Vue from 'Vue'
import Vuex from 'vuex'
Vue.use(Vuex);

//创建存储库
var store = new Vuex.Store({
    state: {
        //用户登录信息
        userlogin:false,
        count: 0,
        //保存所有商品数量信息
        /* AllGoodsInfo: [{
            'data': {
                '001': [{
                    id: '001',
                    enable: true,
                    count: 2,
                    price: 100,
                    title: '1华为 HUAWEI P20 Pro 全面屏徕卡三摄游戏手机 6GB +128GB 宝石蓝 全网通移动联通电信4G手机 双卡双待',
                    imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
                }],
                '002': [{
                    id: '002',
                    enable: true,
                    count: 2,
                    price: 100,
                    title: '联通电信4G手机',
                    imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
                }],
                '003': [{
                    id: '003',
                    enable: true,
                    count: 1,
                    price: 200,
                    title: '华为 HUAWEI P20 Pro ',
                    imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
                }],
                '004': [{
                    id: '004',
                    enable: true,
                    count: 1,
                    price: 300,
                    title: '41111111',
                    imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
                }]
            }
        }] */
        AllGoodsInfo: [
            /* {
                id: '001',
                enable: true,
                count: 2,
                price: 100,
                title: '1华为 HUAWEI P20 Pro 全面屏徕卡三摄游戏手机 6GB +128GB 宝石蓝 全网通移动联通电信4G手机 双卡双待',
                imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
            },
            {
                id: '002',
                enable: true,
                count: 2,
                price: 100,
                title: '联通电信4G手机',
                imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
            },
            {
                id: '003',
                enable: true,
                count: 1,
                price: 200,
                title: '华为 HUAWEI P20 Pro ',
                imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
            },
            {
                id: '004',
                enable: true,
                count: 1,
                price: 300,
                title: '41111111',
                imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
            } */
        ]
    },
    mutations: {
        increment(state, newbuyCount) {
            state.count += newbuyCount;
        },
        //购物车商品添加
        addGoods(state, goods) {

            var newarray = state.AllGoodsInfo.filter(function (item) {
                return item.id == goods.id;
            });

            if (newarray.length) {
                newarray[0].count += goods.count
            } else {
                state.AllGoodsInfo = state.AllGoodsInfo.concat({
                    count: goods.count,
                    id: goods.id,
                    title: goods.title,
                    imgsrc: goods.imgsrc,
                    price: goods.price,
                    enable: goods.enable
                })
            }

            /*
            if (state.AllGoodsInfo[goods.id]) {
                //state.AllGoodsInfo[goods.id] += parseInt(goods.count); 
                state.AllGoodsInfo[goods.id][0].count += goods.count

            } else {
                //state.AllGoodsInfo[goods.id] = parseInt(goods.count); 
                state.AllGoodsInfo[goods.id] = new Array({
                    count: goods.count,
                    id: goods.id,
                    title: goods.title,
                    imgsrc: goods.imgsrc,
                    price: goods.price,
                    enable: goods.enable
                })
            }*/

            console.log('存储状态 AllGoodsInfo：');
            console.log(state.AllGoodsInfo);

            // Vuex 使用 getters，改变 State 中的数组后，getters只触发了一次，后面不会再触发
            // 使用这种方法 https://cn.vuejs.org/v2/api/#Vue-set，触发更新 
            Vue.set(state, new Date().getMilliseconds(), 1);

        },
        //在购物车中更新数量
        update(state, goods) {
            /* if (state.AllGoodsInfo[goods.id]) {
                state.AllGoodsInfo[goods.id][0].count = goods.count
            } */

            var newarray = state.AllGoodsInfo.filter(function (item) {
                return item.id == goods.id;
            });
            if (newarray) {
                newarray[0].count = goods.count
            }
        },
        //更新商品enable
        updateEnable(state, enableList) {

            // state.AllGoodsInfo.forEach(item=>{
            //      item.enable =false;
            // })

            state.AllGoodsInfo.forEach(item => {

                var newarray = enableList.filter(x => {
                    return item.id == x;
                });

                if (newarray.length) {
                    item.enable = true;
                } else
                    item.enable = false;
            })



            console.log(state.AllGoodsInfo);

        },
        //删除商品
        delete(state, id) {
            delete GoodsList[id];
        },
        //获取商品信息
        getGoods(state, id) {
            return GoodsList[id];
        },
        //模拟登录/退出操作
        login(state,isflag){
            state.userlogin = isflag;
            //同时保存到 localStorage
            localStorage.setItem('userlogin',isflag)
        }
    },
    getters: {
        result: function (state) {
            return '结果：' + state.count
        },
        //获得商品总数量
        getTotalQuantity: function (state) {
            console.log('商品数量发生改变');
            var sum = 0;
            state.AllGoodsInfo.forEach(item => {
                // 只计算 enable = true 的商品
                if (item.enable)
                    sum += item.count;
            });
            return sum;
        },
        //获得商品总价格
        getTotalPrice: function (state) {
            console.log('商品总价发生改变');
            var price = 0;
            state.AllGoodsInfo.forEach(item => {
                if (item.enable)
                    price += item.count * item.price;
            });
            return price;
        },
        authorization:function(state){
            
            if(localStorage.getItem('userlogin')!=null)
            {
                state.userlogin = eval(localStorage.getItem('userlogin'));
            }
           
            return state.userlogin;
        }
    }
})

export default store;