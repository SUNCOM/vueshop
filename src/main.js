// 这是项目的JS入口文件

import Vue from 'Vue'

// 导入路由
import VueRouter from 'vue-router'
Vue.use(VueRouter);
// 导入自定义路由模块
import router from './router.js'

/* var store = new Vuex.Store({

}) */
// 导入自定义的vuexStore
import vuexStore from './components/common/vuexStore.js'

// 导入 vue-resource
import VueResource from 'vue-resource'
Vue.use(VueResource);
// 设置普通表单格式
Vue.http.options.emulateJSON = true;
// 设置请求的根路径
// Vue.http.options.root = 'http://127.0.0.1:8888';
// Vue.http.options.root = 'http://192.168.2.101:8888';
Vue.http.options.root='http://140.143.209.230:8888';

// 导入mint-ui  
import 'mint-ui/lib/style.min.css'
// import 'mint-ui/lib/lazyload/style.css'
import { Header, Search ,Swipe, SwipeItem,Toast,Indicator,Button,Lazyload,Loadmore  } from 'mint-ui';
Vue.component(Header.name, Header);
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);
Vue.component(Search.name, Search);
Vue.component(Button.name, Button);
Vue.component(Loadmore.name, Loadmore);
// Vue.use(Lazyload); // ?按需加载 使用 Lazyload ，load加载效果出不来，所以只能全部加载

/* import MintUI from 'mint-ui'
Vue.use(MintUI) */

//导入vue-lazyload 
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: './src//img/404.jpg',
    // error: 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMiAzMiIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiBmaWxsPSJ3aGl0ZSI+CiAgPHBhdGggb3BhY2l0eT0iLjI1IiBkPSJNMTYgMCBBMTYgMTYgMCAwIDAgMTYgMzIgQTE2IDE2IDAgMCAwIDE2IDAgTTE2IDQgQTEyIDEyIDAgMCAxIDE2IDI4IEExMiAxMiAwIDAgMSAxNiA0Ii8+CiAgPHBhdGggZD0iTTE2IDAgQTE2IDE2IDAgMCAxIDMyIDE2IEwyOCAxNiBBMTIgMTIgMCAwIDAgMTYgNHoiPgogICAgPGFuaW1hdGVUcmFuc2Zvcm0gYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIiB0eXBlPSJyb3RhdGUiIGZyb209IjAgMTYgMTYiIHRvPSIzNjAgMTYgMTYiIGR1cj0iMC44cyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIC8+CiAgPC9wYXRoPgo8L3N2Zz4K',
    loading: 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMiAzMiIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiBmaWxsPSJ3aGl0ZSI+CiAgPHBhdGggb3BhY2l0eT0iLjI1IiBkPSJNMTYgMCBBMTYgMTYgMCAwIDAgMTYgMzIgQTE2IDE2IDAgMCAwIDE2IDAgTTE2IDQgQTEyIDEyIDAgMCAxIDE2IDI4IEExMiAxMiAwIDAgMSAxNiA0Ii8+CiAgPHBhdGggZD0iTTE2IDAgQTE2IDE2IDAgMCAxIDMyIDE2IEwyOCAxNiBBMTIgMTIgMCAwIDAgMTYgNHoiPgogICAgPGFuaW1hdGVUcmFuc2Zvcm0gYXR0cmlidXRlTmFtZT0idHJhbnNmb3JtIiB0eXBlPSJyb3RhdGUiIGZyb209IjAgMTYgMTYiIHRvPSIzNjAgMTYgMTYiIGR1cj0iMC44cyIgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIC8+CiAgPC9wYXRoPgo8L3N2Zz4K',
    attempt:1
})

// 导入 MUI 样式
import './lib/mui/css/mui.min.css'
import './lib/mui/css/icons-extra.css'

// 导入时间插件 https://www.npmjs.com/package/vue-moment
//Vue.use(require('vue-moment'));
import moment from 'moment'
//moment.lang('zh-cn');
// 全局时间过滤器
//import _dateFormat from './filter/dateFormat.js'
Vue.filter('dateFormat', function (datestr, pattern = "YYYY-MM-DD HH:mm:ss") {
    if (!datestr) return '';
    return moment(datestr).format(pattern);
})

//价格过滤器
//number为数字类型，是需要转格式的数字，
//places为数字类型，小数点保留的位数，不传默认为2，
//symbol为字符串类型，是货币单位符号，默认为"￥"
Vue.filter('priceFormat', function (number, places=1, symbol) {
    // var pattern = /(?=((?!\b)\d{3})+$)/g;
    // return price.toString().replace(pattern, ',');

    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places: 2;
    //symbol = symbol !== undefined ? symbol: "￥";
    var negative = number < 0 ? "-": "",
    i = parseInt(number = Math.abs( + number || 0).toFixed(places), 10) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return negative + (j ? i.substr(0, j) + ",": "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + ",") + (places ? "." + Math.abs(number - i).toFixed(places).slice(2) : "");
})

// 导入图片预览插件 vue-preview@1.0.5 , 版本1.0.5之后的可能不能自定义标签
import VuePreview from 'vue-preview'
Vue.use(VuePreview)

// vue-picture-preview
// import vuePicturePreview from 'vue-picture-preview'
// Vue.use(vuePicturePreview)

// 导入App根组件
import app from './App.vue'

var vm = new Vue({
    el: '#app',
    data: {

    },
    render: c => c(app),
    router,
    store:vuexStore //将存储对象挂载到vue实例上
})


console.log('测试修改');
console.log('测试修改222222');
console.log('测试修改33');