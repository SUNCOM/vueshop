// node 服务器

var http = require('http');
var url = require('url');
var util = require('util');


const server = http.createServer();
/*
const server =  http.createServer(function (request, response) {

    // 发送 HTTP 头部 
    // HTTP 状态值: 200 : OK
    // 内容类型: text/plain
    response.writeHead(200, {'Content-Type': 'text/plain'});

    

    // 发送响应数据 "Hello World"
    response.end('Hello World\n');
}).listen(8888);*/

// 终端打印如下信息
//console.log('Server running at http://127.0.0.1:8888/');


var GoodsList = [{
        id: '001',
        title: '华为 HUAWEI P20 Pro 全面屏徕卡三摄游戏手机 6GB +128GB 宝石蓝 全网通移动联通电信4G手机 双卡双待',
        price: '99',
        oldprice: '199',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
    },
    {
        id: '002',
        title: '佳能（Canon）EOS 80D 单反套机（EF-S 18-200mm f/3.5-5.6 IS） 2420万有效像素 45点十字对焦 WIFI/NFC',
        price: '8699',
        oldprice: '8296',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n1/s546x546_jfs/t2656/295/34058120/362134/d92995e5/56fc835dNe349b797.jpg'
    },
    {
        id: '003',
        title: '微软（Microsoft） 新New Surface Pro 5 笔记本平板电脑二合一 轻薄超级本 Pro5 i7 16G内存 1TB固态硬盘 官方标配+原装键盘',
        price: '18488',
        oldprice: '20000',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t24124/177/709571419/289070/1a4aaa6d/5b3d7bf7N4333990b.jpg'
    },
    {
        id: '004',
        title: '微软（Microsoft）Surface Book 2 二合一平板笔记本 15英寸（Intel i7 16G内存 1T存储）银色',
        price: '26488',
        oldprice: '28000',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t18448/214/581406193/459791/4b487315/5a968786Nf4413c56.jpg'
    },
    {
        id: '005',
        title: '华为 HUAWEI P20 Pro 全面屏徕卡三摄游戏手机 6GB +128GB 宝石蓝 全网通移动联通电信4G手机 双卡双待',
        price: '99',
        oldprice: '199',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t19225/206/1269058000/160653/b8379287/5ac1e879N3153c619.jpg'
    },
    {
        id: '006',
        title: '澳大利亚进口脐橙 澳橙12个装 单果重约150g-180g 新鲜水果',
        price: '39.9',
        oldprice: '99',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t21793/211/610710429/182079/477ff354/5b431a5cNd434d35f.jpg'
    },
    {
        id: '007',
        title: '云南白药 人气3+2牙膏套装 (留兰香180g+薄荷清爽185g+冬青香170g+2支牙刷)牙刷随机发货',
        price: '77',
        oldprice: '199',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t2854/260/482062549/390253/45fa2b74/5716007eNe74dd92e.jpg'
    },
    {
        id: '008',
        title: 'HTC U12+ VR互联产品 陶瓷黑 6GB+128GB 移动联通电信全网通',
        price: '5588',
        oldprice: '6000',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t20332/316/997260601/64637/108d1e54/5b1e54f7N486ce8b8.jpg'
    },
    {
        id: '009',
        title: '宏碁（Acer）墨舞TX50 15.6英寸笔记本（i5-7200U 8G DDR4 256GB SSD 940MX 2G DDR5显存 全高清）黑色',
        price: '3999',
        oldprice: '5800',
        yu: '200',
        imgsrc: 'https://img14.360buyimg.com/n0/jfs/t10273/45/1863613262/167090/45a76989/59e84aedN5079c46a.jpg'
    },
    {
        id: '100',
        title: 'COACH 蔻驰女包女士时尚波士顿桶单肩斜跨手提包女 黑色 F57521IMBLK',
        price: '1145',
        oldprice: '1680',
        yu: '200',
        imgsrc: 'https://img11.360buyimg.com/n5/s450x450_jfs/t22993/85/1429681538/194646/d189adc7/5b605ddfN982ae1cc.jpg'
    }
]


server.on('request', function (req, res) {

    res.writeHead(200, {
        'Content-Type': 'text/plain',
        'Access-Control-Allow-Origin': '*'
    }); //
    //res.writeHead(200,{'Content-Type':'application/json;charset=utf-8','Access-Control-Allow-Origin':'*'});

    //response.setHeader("Access-Control-Allow-Origin", "*"); 

    // 解析 url 参数
    //var params = url.parse(res.url, true).query;
    // response.write("网站名：" + params.name);
    // response.write("\n");
    // response.write("网站 URL：" + params.url);
    // response.end();

    let _url = req.url.toLowerCase();

    //轮播图
    if (/getbannerlist/.test(_url)) {

        var message = [{
                imgurl: 'https://img.alicdn.com/tfs/TB1HVHCH21TBuNjy0FjXXajyXXa-520-280.jpg_q90_.webp'
            },
            {
                imgurl: 'https://img.alicdn.com/simba/img/TB1FkSSlHZnBKNjSZFGSuvt3FXa.jpg'
            },
            {
                imgurl: 'https://img.alicdn.com/simba/img/TB1FcDOgL9TBuNjy0FcSuveiFXa.jpg'
            },
            {
                imgurl: 'https://img.alicdn.com/tfs/TB140OqHhGYBuNjy0FnXXX5lpXa-520-280.jpg_q90_.webp'
            }
        ]
        var callback = JSON.stringify(getJSON_obj(message));
        res.end(callback);
    }
    //新闻列表
    else if (/getnewlist/.test(_url)) {
        var message = [{
                id: '2333d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: '111',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: 'b23d',
                title: '12321dsacxzcxz',
                author: '央视网',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2s3d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: 'dsa',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2d3d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: 'xz',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '232d',
                title: '23333333333333333',
                author: '央视网',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2cer3d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: '央视网',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2c3d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: 'cc',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2r3bd',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: 'AA',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2fvd3d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: '央视网',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2v3d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: 'c',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '23vcd',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: 'c',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '23vcxd',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: '央视网',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            },
            {
                id: '2yy3d',
                title: '中国这条高铁被誉为最会赚钱的高铁 像公交车一样5分钟就发一趟',
                author: '央视网',
                date: '2018-7-29 13:54:53',
                comment: '1.5万评',
                imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg'
            }
        ];
        var callback = JSON.stringify(getJSON_obj(message));
        res.end(callback);
    }
    // 获取新闻详情
    else if (_url === "/api/getnewinfo/") {

        var message = {
            id: '2yy3d',
            title: '能和心爱的人一起睡觉，是件幸福的事情；可是，打呼噜怎么办？',
            author: '央视网',
            date: '2018-7-29 13:54:53',
            comment: '1.5万评',
            imgurl: 'http://f.hiphotos.baidu.com/image/pic/item/63d0f703918fa0ece5f167da2a9759ee3d6ddb37.jpg',
            content: '11111111111111111111111111111111111111111111111'
        };
        var callback = JSON.stringify(getJSON_obj(message));
        res.end(callback);

    }
    //获得商品列表
    else if (/getgoodslist/.test(_url)) {

        var callback = JSON.stringify(getJSON_obj(GoodsList));
        res.end(callback);      
    } 
    //获得商品详情
    else if (/getgoodinfo/.test(_url)) {

        var params = url.parse(req.url, true).query;  //parse将字符串转成对象,req.url="/?url=123&name=321"，true表示params是{url:"123",name:"321"}，false表示params是url=123&name=321
        // res.write("网站名：" + params.name);
        // res.write("\n");
        // res.write("网站 URL：" + params.url);
        // res.end();

        var  message=null;
        GoodsList.some(function(item){
            if(item.id==params.id)
            {
                message=item;
                return true
            }
            else{
                return false;
            }
        })

        var callback = JSON.stringify(getJSON_obj(message));
        res.end(callback);      
    }
    else if (_url === "/api/getgoodslist/") {
        res.end('1111111111111111111111');
    }
    else {
        //res.end('接口调用失败');
 




    }
    //res.end('111111');
    // res.end(params.pathname);
    // console.log(params)


           

           
})


server.listen(8888, function () {
    console.log('Server running at http://127.0.0.1:8888/');
})


function getJSON_obj(m) {
    return {
        status: 0,
        message: m
    };
}